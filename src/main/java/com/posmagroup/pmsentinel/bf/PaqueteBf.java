package com.posmagroup.pmsentinel.bf;

import com.posmagroup.pmsentinel.models.Paquete;
import java.util.List;

/**
 * Interfaz de fachada de negocio correspondiente a Paquete
 * @author Posmagroup
 */
public interface PaqueteBf {
    Paquete savePaquete(Paquete paquete);

    List<Paquete> findAllPaquetes();

    void deletePaquete(long id);

    Paquete findByIdPaquete(long id);

    Paquete updatePaquete(Paquete paquete);
    
    Paquete relocatePaquete(Paquete paquete, Integer idOficina);
    
    Paquete delegatePaquete(Paquete paquete, Integer idUsuario);
    
    List<Paquete> getPackagesByOfficeId(Integer idOficina);
}
