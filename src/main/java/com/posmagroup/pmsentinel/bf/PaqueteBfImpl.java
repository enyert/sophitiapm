package com.posmagroup.pmsentinel.bf;

import com.posmagroup.pmsentinel.dao.PaqueteDao;
import com.posmagroup.pmsentinel.models.Paquete;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementacion de la clase de fachada de negocio PaqueteBF
 * @author Posmagroup
 */
@Transactional
@Service("paqueteBf")
public class PaqueteBfImpl implements PaqueteBf{

    final static Logger logger = Logger.getLogger(PaqueteBfImpl.class);
    
    @Autowired
    PaqueteDao paqueteDao;
    
    @Override
    public Paquete savePaquete(Paquete paquete) {
        logger.info("Entrando a la clase " +  getClass().getName() + "- Metodo savePaquete");
        return paqueteDao.create(paquete);
    }

    @Override
    public List<Paquete> findAllPaquetes() {
        logger.info("Entrando a la clase " +  getClass().getName() + "- Metodo findAllPaquetes");
        return paqueteDao.findAll();
    }

    @Override
    public void deletePaquete(long id) {
        logger.info("Entrando a la clase " +  getClass().getName() + "- Metodo deletePaquete");
        paqueteDao.deleteById(id);
    }

    @Override
    public Paquete findByIdPaquete(long id) {
        logger.info("Entrando a la clase " +  getClass().getName() + "- Metodo findByIdPaquete");
        return paqueteDao.findOne(id);
    }

    @Override
    public Paquete updatePaquete(Paquete paquete) {
        logger.info("Entrando a la clase " +  getClass().getName() + "- Metodo findByIdPaquete");
        return paqueteDao.update(paquete);
    }
    
    @Override
    public Paquete relocatePaquete(Paquete paquete, Integer idOficina){
        logger.info("Entrando a la clase " +  getClass().getName() + "- Metodo relocatePaquete");
        paquete.setIdOficina(idOficina);
        return paqueteDao.update(paquete);
    }

    @Override
    public Paquete delegatePaquete(Paquete paquete, Integer idUsuario) {
        logger.info("Entrando a la clase " +  getClass().getName() + "- Metodo delegatePaquete");
        paquete.setIdUsuario(idUsuario);
        return paqueteDao.update(paquete);
    }
    
    @Override
    public List<Paquete> getPackagesByOfficeId(Integer oficinaId){
        logger.info("Entrando a la clase " +  getClass().getName() + "- getPackagesByOfficeId");
        return paqueteDao.findAllByFieldId("idOficina", oficinaId.longValue());
    }
}
