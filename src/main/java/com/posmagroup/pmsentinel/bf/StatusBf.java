package com.posmagroup.pmsentinel.bf;

import com.posmagroup.pmsentinel.models.Status;
import java.util.List;

/**
 * Interfaz de fachada de negocio correspondiente a la clase Status
 * @author Posmagroup
 */
public interface StatusBf {
    Status saveStatus(Status status);

    List<Status> findAllStatus();

    void deleteStatus(long id);

    Status findByIdStatus(long id);

    Status updateStatus(Status status);
}
