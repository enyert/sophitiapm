package com.posmagroup.pmsentinel.bf;

import com.posmagroup.pmsentinel.dao.StatusDao;
import com.posmagroup.pmsentinel.models.Status;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementacion de la clase de fachada de negocio StatusBf
 * @author Posmagroup
 */
@Transactional 
@Service("statusService")
public class StatusBfImpl implements StatusBf{

    final static Logger logger = Logger.getLogger(StatusBfImpl.class);
    
    @Autowired
    StatusDao statusDao;
    
    @Override
    public Status saveStatus(Status status) {
        return statusDao.create(status);
    }

    @Override
    public List<Status> findAllStatus() {
        return statusDao.findAll();
    }

    @Override
    public void deleteStatus(long id) {
        statusDao.deleteById(id);
    }

    @Override
    public Status findByIdStatus(long id) {
        return statusDao.findOne(id);
    }

    @Override
    public Status updateStatus(Status status) {
        return statusDao.update(status);
    }
    
}
