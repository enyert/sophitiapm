package com.posmagroup.pmsentinel.bf;

import com.posmagroup.pmsentinel.models.Tarea;
import java.util.List;

/**
 * Interfaz de fachada de negocio correspondiente a la clase Tarea
 * @author Posmagroup
 */
public interface TareaBf {
    Tarea saveTarea(Tarea tarea);

    List<Tarea> findAllTareas();

    void deleteTarea(long id);

    Tarea findByIdTarea(long id);

    Tarea updateTarea(Tarea tarea);
}
