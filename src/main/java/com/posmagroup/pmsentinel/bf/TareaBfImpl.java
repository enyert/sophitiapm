/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.posmagroup.pmsentinel.bf;

import com.posmagroup.pmsentinel.dao.TareaDao;
import com.posmagroup.pmsentinel.models.Tarea;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementacion de la clase de fachada de negocio TareaBF
 * @author Posmagroup
 */
@Transactional
@Service("tareaService")
public class TareaBfImpl implements TareaBf{

    final static Logger logger = Logger.getLogger(StatusBfImpl.class);
    
    @Autowired
    TareaDao tareaDao;
    
    @Override
    public Tarea saveTarea(Tarea tarea) {
        return tareaDao.create(tarea);
    }

    @Override
    public List<Tarea> findAllTareas() {
        return tareaDao.findAll();
    }

    @Override
    public void deleteTarea(long id) {
        tareaDao.deleteById(id);
    }

    @Override
    public Tarea findByIdTarea(long id) {
        return tareaDao.findOne(id);
    }

    @Override
    public Tarea updateTarea(Tarea tarea) {
        return tareaDao.update(tarea);
    }
    
}
