/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.posmagroup.pmsentinel.bf.util;

import com.posmagroup.pmsentinel.models.Paquete;
import org.springframework.stereotype.Service;

/**
 *
 * @author Posmagroup
 */
@Service("generalSentinel")
public class GeneralSentinel {
    
    public boolean isFromAtencionComunitaria(Paquete paquete){
        return paquete.getOrigen().equals("AC");
    }
}
