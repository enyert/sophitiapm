package com.posmagroup.pmsentinel.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Clase de configuración principal
 * @author Posmagroup
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.posmagroup.pmsentinel")
public class AppConfig{
   
}
