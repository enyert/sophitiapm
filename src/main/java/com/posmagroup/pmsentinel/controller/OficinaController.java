package com.posmagroup.pmsentinel.controller;

import com.posmagroup.pmsentinel.bf.PaqueteBf;
import com.posmagroup.pmsentinel.models.Paquete;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controlador para las oficinas
 * @author Posmagroup
 */
@RestController
public class OficinaController {
    
    @Autowired
    PaqueteBf paqueteBf;
    
    @RequestMapping(value = "/oficina/{id}/paquete", method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Paquete> getPaquetesByOfficeId(@PathVariable long id){
        return paqueteBf.getPackagesByOfficeId(new Long(id).intValue());
    }
    
    
}
