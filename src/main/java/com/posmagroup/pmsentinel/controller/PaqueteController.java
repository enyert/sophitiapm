package com.posmagroup.pmsentinel.controller;

import com.posmagroup.pmsentinel.bf.PaqueteBf;
import com.posmagroup.pmsentinel.models.Paquete;
import com.posmagroup.pmsentinel.service.clients.NotaMarginalClient;
//import com.posmagroup.pmsentinel.service.clients.Prueba;
import java.util.List;
import javax.json.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controlador REST para los paquetes
 * @author Posmagroup
 */
@RestController
public class PaqueteController {
    
    @Autowired
    PaqueteBf paqueteBf;
    
    
    @RequestMapping(value = "/paquete/{id}", method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Paquete getPaqueteById(@PathVariable long id){
        return paqueteBf.findByIdPaquete(id);
    }
    
    @RequestMapping(value = "/paquete", method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Paquete> getAllPaquetes(){
        return paqueteBf.findAllPaquetes();
    }
    
    @RequestMapping(value = "/paquete", method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Paquete updatePaquete(@RequestBody Paquete paquete){
        //Prueba prueba = new Prueba();
        
        //JsonObject jo = paquete.getPayload();
        //int number = Integer.parseInt(jo.getString("value"));
        
        //paquete.setNextStep(prueba.mapper(paquete.getNextStep(), number));
        
        return paqueteBf.updatePaquete(paquete);
    }
    
    @RequestMapping(value = "/paquete", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Paquete savePaquete(@RequestBody Paquete paquete){
        NotaMarginalClient client = new NotaMarginalClient();
        paquete.setNextStep(client.calculateNext(paquete.getNextStep()));
        
        return paqueteBf.savePaquete(paquete);
    }
    
    
    @RequestMapping(value = "/paquete/{id}/oficina/{oficinaId}", method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public Paquete relocate(Paquete paquete, long oficinaId){
        
        return paqueteBf.relocatePaquete(paquete, (int)oficinaId);
    }
    
    
}
