package com.posmagroup.pmsentinel.controller;

import com.posmagroup.pmsentinel.bf.StatusBf;
import com.posmagroup.pmsentinel.models.Status;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controlador REST para status
 * @author Posmagroup
 */
@RestController
public class StatusController {
    
    @Autowired
    StatusBf statusBf;
    
    
    @RequestMapping(value = "/status/{id}", method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Status getStatusById(@PathVariable long id){
        return statusBf.findByIdStatus(id);
    }
    
    @RequestMapping(value = "/status", method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Status> getAllStatus(){
        return statusBf.findAllStatus();
    }
    
    @RequestMapping(value = "/status", method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Status updateStatus(@RequestBody Status status){
        return statusBf.updateStatus(status);
    }
    
    @RequestMapping(value = "/status", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Status saveStatus(@RequestBody Status status){
        return statusBf.saveStatus(status);
    }    
}
