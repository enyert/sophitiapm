/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.posmagroup.pmsentinel.controller;

import com.posmagroup.pmsentinel.bf.TareaBf;
import com.posmagroup.pmsentinel.models.Tarea;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controlador REST para las tareas
 * @author Posmagroup
 */
@RestController
public class TareaController {
    
    @Autowired 
    TareaBf tareaBf;
    
    @RequestMapping(value = "/tarea/{id}", method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Tarea getTareaById(@PathVariable long id){
        return tareaBf.findByIdTarea(id);
    }
    
    @RequestMapping(value = "/tarea", method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Tarea> getAllTareas(){
        return tareaBf.findAllTareas();
    }
    
    @RequestMapping(value = "/tarea", method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Tarea updateTarea(@RequestBody Tarea tarea){
        return tareaBf.updateTarea(tarea);
    }
    
    @RequestMapping(value = "/tarea", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Tarea saveTarea(@RequestBody Tarea tarea){
        return tareaBf.saveTarea(tarea);
    }
}
