/***
 * Clase abstracta AbstractDao que define los metodos genéricos de las clases
 * de tipo Dao.
 */
package com.posmagroup.pmsentinel.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.List;


    public abstract class AbstractDao<T extends Serializable> {

    private Class<T> clazz;

    @PersistenceContext
    private EntityManager entityManager;
    
    public EntityManager getEntityManager(){
        return this.entityManager;
    }

    public final void setClazz(final Class<T> clazzToSet) {
        this.clazz = clazzToSet;
    }

    public T findOne(final long id) {
        return entityManager.find(clazz, id);
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll() {
        return entityManager.createQuery("from " + clazz.getName()).getResultList();
    }
    
    @SuppressWarnings("unchecked")
    public List<T> findAllByFieldId(String fieldName, long id){
        return entityManager.createQuery("from " + clazz.getName() + " where " + fieldName + "=" + id).getResultList();
    }
    
    public T create(final T entity) {
        entityManager.persist(entity);
        return entity;
    }

    public T update(final T entity) {
        return entityManager.merge(entity);
    }

    public void delete(final T entity) {
        entityManager.remove(entity);
    }

    public void deleteById(final long entityId) {
        final T entity = findOne(entityId);
        delete(entity);
    }
    
    
}
