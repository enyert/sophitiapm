/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.posmagroup.pmsentinel.dao;

import com.posmagroup.pmsentinel.models.Paquete;
import java.util.List;

/**
 *
 * @author Posmagroup
 */
public interface PaqueteDao {
    Paquete findOne(long id);
    
    List<Paquete> findAll();
    
    Paquete create(Paquete paquete);
    
    Paquete update(Paquete paquete);
    
    void delete (Paquete paquete);
    
    void deleteById(long entityId);
    
    public List<Paquete> findAllByFieldId(String fieldName, long id);
}
