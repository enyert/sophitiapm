package com.posmagroup.pmsentinel.dao;

import com.posmagroup.pmsentinel.models.Paquete;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Posmagroup
 */
@Repository("paqueteDao")
public class PaqueteDaoImpl extends AbstractDao<Paquete> implements PaqueteDao {

    public PaqueteDaoImpl() {
        super();
        setClazz(Paquete.class);
    }
}
