/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.posmagroup.pmsentinel.dao;

import com.posmagroup.pmsentinel.models.Status;
import java.util.List;

/**
 *
 * @author Posmagroup
 */
public interface StatusDao {
    Status findOne(long id);
    
    List<Status> findAll();
    
    Status create(Status tipoNotaMarginal);
    
    Status update(Status tiponotaMarginal);
    
    void delete (Status tipoNotaMarginal);
    
    void deleteById(long entityId);
}
