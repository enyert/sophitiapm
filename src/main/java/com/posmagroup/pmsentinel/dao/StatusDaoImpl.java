/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.posmagroup.pmsentinel.dao;

import com.posmagroup.pmsentinel.models.Status;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Posmagroup
 */
@Repository("statusDao")
public class StatusDaoImpl extends AbstractDao<Status> implements StatusDao{

    public StatusDaoImpl() {
        super();
        setClazz(Status.class);
    }
}
