/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.posmagroup.pmsentinel.dao;

import com.posmagroup.pmsentinel.models.Tarea;
import java.util.List;

/**
 *
 * @author Posmagroup
 */
public interface TareaDao {
    Tarea findOne(long id);
    
    List<Tarea> findAll();
    
    Tarea create(Tarea tipoNotaMarginal);
    
    Tarea update(Tarea tiponotaMarginal);
    
    void delete (Tarea tipoNotaMarginal);
    
    void deleteById(long entityId);
}
