/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.posmagroup.pmsentinel.dao;

import com.posmagroup.pmsentinel.models.Tarea;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Posmagroup
 */
@Repository("tareaDao")
public class TareaDaoImpl extends AbstractDao<Tarea> implements TareaDao{

    public TareaDaoImpl() {
        super();
        setClazz(Tarea.class);
    }
    
}
