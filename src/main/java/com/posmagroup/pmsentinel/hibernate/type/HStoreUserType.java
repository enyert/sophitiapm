package com.posmagroup.pmsentinel.hibernate.type;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;

/**
 * Tipo de dato creado por el usuario para usar JSON de tipo key, value
 * @author Posmagroup
 */
public class HStoreUserType implements UserType{
    
    public Object assemble(Serializable cached, Object owner) throws HibernateException{
        return cached;
    }
    
    public Object deepCopy(Object o) throws HibernateException{
        Map m  = (Map) o;
        return new HashMap(m);
    }
    
    public Serializable disassemble(Object o) throws HibernateException{
        return (Serializable) o;
    }
    
    public boolean equals(Object o1, Object o2){
        Map m1 = (Map) o1;
        Map m2 = (Map) o2;
        return m1.equals(o2);
    }
    
    public int hashCode(Object o) throws HibernateException{
        return o.hashCode();
    }
    
    @Override
    public Object nullSafeGet(ResultSet resultSet, String[] strings, SessionImplementor sessionImplementor, Object o) throws HibernateException, SQLException{
        String col = strings[0];
        String val = resultSet.getString(col);
        return HStoreHelper.toMap(val);
    }
    
    @Override
    public void nullSafeSet(PreparedStatement preparedStatement, Object o, int i, SessionImplementor sessionImplementor) throws HibernateException, SQLException {
        String s = HStoreHelper.toString((Map) o);
        preparedStatement.setObject(i, s, Types.OTHER);
    }
    
    public boolean isMutable() {
        return true;
    }
    
    public Object replace(Object original, Object target, Object owner)
            throws HibernateException {
        return original;
    }
    
    public Class returnedClass() {
        return Map.class;
    }
    
    public int[] sqlTypes() {
        return new int[] { Types.INTEGER };
    }
}
