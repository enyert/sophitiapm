/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.posmagroup.pmsentinel.hibernate.type;

import java.sql.Types;
import org.hibernate.dialect.PostgreSQLDialect;

/**
 *
 * @author Posmagroup
 */
public class JsonPostgreSQLDialect extends PostgreSQLDialect{

    public JsonPostgreSQLDialect() {
        super();
        
        this.registerColumnType(Types.JAVA_OBJECT, "json");
    }
}
