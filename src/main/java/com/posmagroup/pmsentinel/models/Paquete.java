package com.posmagroup.pmsentinel.models;

import com.google.gson.Gson;
import com.posmagroup.pmsentinel.hibernate.type.JsonPostgresUserType;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.json.JsonObject;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

/**
 * Entidad de Paquete
 * @author Posmagroup
 */
@Entity
@TypeDef(name="StringJsonObject", typeClass = JsonPostgresUserType.class)
public class Paquete implements Serializable{
    @Id
    @GeneratedValue
    private Long id;
    
    @Column(nullable=false)
    private String nombre;
    
    @Column(nullable=false)
    private String origen;
        
    /*@Type(type="StringJsonObject")
    @Column(columnDefinition="json")
    private JsonObject payload; */

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "status_id")
    private Status status;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "tarea_id")
    private Tarea tarea;
    
    @Column(nullable=false)
    private String statusSolicitud;
    
    @Column(nullable=false)
    private Integer idOficina;
    
    @Column(nullable=true)
    private Integer idUsuario;
    
    @Column(nullable = false)
    private String nextStep;
    
    public Paquete() {
    }

    public Paquete(String nombre, String origen, JsonObject payload, Status status, Tarea tarea, String statusSolicitud, Integer idOficina, Integer idUsuario, String nextStep) {
        this.nombre = nombre;
        this.origen = origen;
        this.status = status;
        this.tarea = tarea;
        this.statusSolicitud = statusSolicitud;
        //this.payload = payload;
        this.idOficina = idOficina;
        this.idUsuario = idUsuario;
        this.nextStep = nextStep;
    }

    /*public JsonObject getPayload() {
        return payload;
    }*/

    /*public void setPayload(JsonObject payload) {
        this.payload = payload;
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Tarea getTarea() {
        return tarea;
    }

    public void setTarea(Tarea tarea) {
        this.tarea = tarea;
    }

    public String getStatusSolicitud() {
        return statusSolicitud;
    }

    public void setStatusSolicitud(String statusSolicitud) {
        this.statusSolicitud = statusSolicitud;
    }

    public Integer getIdOficina() {
        return idOficina;
    }

    public void setIdOficina(Integer idOficina) {
        this.idOficina = idOficina;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public String toString() {
        //return new Gson().toJson(this);
        return "Paquete{" + "id=" + id + ", nombre=" + nombre + ", status=" + status + ", tarea=" + tarea + ", statusSolicitud=" + statusSolicitud + ", idOficina=" + idOficina + ", idUsuario=" + idUsuario + '}';
    }

    public String getNextStep() {
        return nextStep;
    }

    public void setNextStep(String nextStep) {
        this.nextStep = nextStep;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }
    
    
}
