/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.posmagroup.pmsentinel.models;

import com.google.gson.Gson;
import java.io.Serializable;
import javax.json.Json;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Entidad que corresponde a Permiso
 * @author Posmagroup
 */
@Entity
public class Permiso implements Serializable {
    @Id
    private Long id;
    private String nombre;
    private String descripcion;

    public Permiso() {
    }

    public Permiso(String nombre, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        //return new Gson().toJson(this);
        return "Permiso{" + "id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + '}';
    }
    
    
}
