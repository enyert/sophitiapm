/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.posmagroup.pmsentinel.service.clients;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Posmagroup
 */
public class NotaMarginalClient {
    
    private final Map<String, String> states;

    public NotaMarginalClient() {
        this.states = new HashMap<>();
        initializeStates();
    }
     
    
    public String calculateNext(String actual){
        return states.get(actual);
    }
    
   public final void initializeStates(){
       states.put("notasmarginales", "notasmarginales.seleccionarnotamarginal");
       states.put("notasmarginales.seleccionarnotamarginal", "notasmarginales.cargarrecaudos");
       states.put("notasmarginales.cargarrecaudos", "notasmarginales.buscaracta");
   }
}
